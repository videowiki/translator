const SchemaNames = {
    commentsThread: 'commentsThread',
    article: 'article',
    organization: 'organization',
    translationRequest: 'translationRequest',
    user: 'user',
    video: 'video',
    orgRole: 'orgRole',
};

module.exports = {
    SchemaNames,
};
